module Utils exposing (cnext, cprev, get)

import List


cnext : Int -> List a -> Int
cnext i list =
    if i < 0 || i >= List.length list - 1 then
        0

    else
        i + 1


cprev : Int -> List a -> Int
cprev i list =
    if i <= 0 || i >= List.length list then
        List.length list - 1

    else
        i - 1


get : Int -> List a -> Maybe a
get i list =
    if i >= List.length list || i < 0 then
        Nothing

    else
        List.head <| List.drop i list
