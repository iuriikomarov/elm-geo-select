module View exposing (view)

import Css exposing (..)
import Geocode.Response
import Html.Styled as T exposing (Attribute, Html)
import Html.Styled.Attributes as A
import Html.Styled.Events exposing (..)
import Json.Decode as Json
import List
import State exposing (Model, Msg(..))


type alias Element =
    Html Msg


type alias Elements =
    List (Html Msg)


colors =
    { main = rgb 80 80 80
    , accent = rgb 68 121 251
    , accent2 = rgba 68 121 251 0.1
    , accent3 = rgba 108 161 251 0.1
    , white = rgb 255 255 255
    }


styleFont : List Style
styleFont =
    [ fontFamilies [ "Roboto", "Helvetica Neue", "sans-serif" ]
    , fontSize (px 16)
    , fontWeight normal
    , color colors.main
    ]


input : String -> Element
input v =
    T.input
        [ A.placeholder "Text to search"
        , A.value v
        , A.autofocus True
        , onInput Input
        , onBlur (SetFocus False)
        , onFocus (SetFocus True)
        , on "keydown" (Json.map KeyPressed keyCode)
        , A.css <|
            styleFont
                ++ [ padding2 (px 15) (px 20)
                   , borderWidth zero
                   , display block
                   , boxSizing borderBox
                   , position relative
                   , zIndex (int 2)
                   , width (pct 100)
                   , margin zero
                   , focus
                        [ color colors.accent
                        ]
                   ]
        ]
        []


suggestions : Elements -> Element
suggestions =
    T.div
        [ A.css
            [ position absolute
            , top (pct 100)
            , left zero
            , right zero
            , backgroundColor (rgb 255 255 255)
            , boxShadow5 (px 0) (px 5) (px 10) (px -3) (rgba 133 131 133 0.85)
            , zIndex (int 1)
            , borderRadius4 zero zero (px 5) (px 5)
            ]
        ]


suggestion : ( Geocode.Response.Suggestion, Bool ) -> Element
suggestion ( ( location, verbose ), active ) =
    let
        baseStyles =
            [ padding2 (px 15) (px 20)
            , color colors.main
            , cursor pointer
            , hover
                [ backgroundColor colors.accent3
                ]
            ]

        styles =
            if not active then
                baseStyles

            else
                baseStyles
                    ++ [ color colors.accent
                       , backgroundColor colors.accent2
                       ]
    in
    T.div
        [ onClick (ShowMarkerOnMap location)
        , A.css styles
        ]
        [ T.text verbose ]


root : Elements -> Element
root =
    T.div
        [ A.id "select"
        , A.css <|
            styleFont
                ++ [ position relative
                   ]
        ]


view : Model -> Element
view m =
    let
        mapper i s =
            case m.selectedSuggestion of
                Nothing ->
                    ( s, False )

                Just j ->
                    ( s, j == i )
    in
    root
        [ input m.input
        , suggestions <|
            List.map suggestion <|
                List.indexedMap mapper m.suggestions
        ]
