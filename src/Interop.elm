port module Interop exposing (showMarkerOnMap)

import Geocode.Response exposing (Location)


port showMarkerOnMap : Location -> Cmd msg
