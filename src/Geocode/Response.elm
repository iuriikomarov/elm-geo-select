module Geocode.Response exposing (Location, Viewport, Suggestion, parse)

import Json.Decode as Decode exposing (Decoder, field, float, list, maybe, string)
import Json.Decode.Pipeline exposing (optional, required)


type alias Location =
    { lat : Float
    , lng : Float
    }


locationDecoder : Decoder Location
locationDecoder =
    Decode.succeed Location
        |> required "lat" float
        |> required "lng" float


type alias Viewport =
    { northeast : Location
    , southwest : Location
    }


viewportDecoder : Decoder Viewport
viewportDecoder =
    Decode.succeed Viewport
        |> required "northeast" locationDecoder
        |> required "southwest" locationDecoder


type LocationType
    = Rooftop
    | RangeInterpolated
    | GeometricCenter
    | Approximate
    | UnknownLocationType


locationTypeDecoder : Decoder LocationType
locationTypeDecoder =
    string
        |> Decode.andThen
            (\str ->
                case str of
                    "ROOFTOP" ->
                        Decode.succeed Rooftop

                    "RANGE_INTERPOLATED" ->
                        Decode.succeed RangeInterpolated

                    "GEOMETRIC_CENTER" ->
                        Decode.succeed GeometricCenter

                    "APPROXIMATE" ->
                        Decode.succeed Approximate

                    _ ->
                        Decode.succeed UnknownLocationType
            )


type alias AddressComponentType =
    String


addressComponentTypeDecoder : Decoder AddressComponentType
addressComponentTypeDecoder =
    string


type alias AddressComponent =
    { long_name : String
    , short_name : String
    , types : List AddressComponentType
    }


addressComponentDecoder : Decoder AddressComponent
addressComponentDecoder =
    Decode.succeed AddressComponent
        |> required "long_name" string
        |> required "short_name" string
        |> required "types" (list string)


type Status
    = StatusOk
    | ZeroResults
    | OverDailyLimit
    | OverQueryLimit
    | RequestDenied
    | InvalidRequest
    | UnknownError


statusDecoder : Decoder Status
statusDecoder =
    string
        |> Decode.andThen
            (\str ->
                case str of
                    "OK" ->
                        Decode.succeed StatusOk

                    "ZERO_RESULTS" ->
                        Decode.succeed ZeroResults

                    "OVER_DAILY_LIMIT" ->
                        Decode.succeed OverDailyLimit

                    "OVER_QUERY_LIMIT" ->
                        Decode.succeed OverQueryLimit

                    "REQUEST_DENIED" ->
                        Decode.succeed RequestDenied

                    "INVALID_REQUEST" ->
                        Decode.succeed InvalidRequest

                    _ ->
                        Decode.succeed UnknownError
            )


type alias Geometry =
    { location : Location
    , location_type : LocationType
    , viewport : Viewport
    }


geometryDecoder : Decoder Geometry
geometryDecoder =
    Decode.succeed Geometry
        |> required "location" locationDecoder
        |> required "location_type" locationTypeDecoder
        |> required "viewport" viewportDecoder


type alias ApiResult =
    { geometry : Geometry
    , address_components : List AddressComponent
    , formatted_address : String
    , place_id : String
    }


apiResultDecoder : Decoder ApiResult
apiResultDecoder =
    Decode.succeed ApiResult
        |> required "geometry" geometryDecoder
        |> required "address_components" (list addressComponentDecoder)
        |> required "formatted_address" string
        |> required "place_id" string


type alias Response =
    { error_message : Maybe String
    , status : Status
    , results : List ApiResult
    }


responseDecoder : Decoder Response
responseDecoder =
    Decode.succeed Response
        |> optional "error_message" (maybe string) Nothing
        |> required "status" statusDecoder
        |> optional "results" (list apiResultDecoder) []


transform : Response -> List Suggestion
transform { status, results } =
    if status /= StatusOk then
        []

    else
        List.map (\r -> ( r.geometry.location, r.formatted_address )) results


parse : String -> List Suggestion
parse str =
    let
        decoded =
            Decode.decodeString responseDecoder str
    in
    case decoded of
        Err _ ->
            []

        Ok response ->
            transform response


type alias Suggestion =
    ( Location, String )
