module Geocode.Request exposing (request)

import Http
import State exposing (..)


apiKey : Model -> String
apiKey model =
    model.flags.apiKey


request : Model -> String -> Cmd Msg
request model str =
    Http.get
        { url = "https://maps.googleapis.com/maps/api/geocode/json?address=" ++ str ++ "&key=" ++ apiKey model
        , expect = Http.expectString ProcessApiResponse
        }
