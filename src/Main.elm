port module Main exposing (main)

import Browser
import Html.Styled exposing (toUnstyled)
import Platform.Sub
import State exposing (..)
import Update exposing (update)
import View exposing (view)


subscriptions : Model -> Sub Msg
subscriptions model =
    Platform.Sub.batch []


init : Flags -> ( Model, Cmd Msg )
init f =
    ( initialModel f, initialCommand f )


main =
    Browser.element
        { init = init
        , view = view >> toUnstyled
        , update = update
        , subscriptions = subscriptions
        }
