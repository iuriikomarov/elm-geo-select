module Update exposing (update)

import Debounce as D
import Debug
import Geocode.Request
import Geocode.Response exposing (Suggestion, parse)
import Http
import Interop
import Platform.Cmd exposing (none)
import State exposing (Model, Msg(..), resetModel, resetSuggestions)
import String
import Task
import Utils


debounceConfig : D.Config Msg
debounceConfig =
    { strategy = D.later 200
    , transform = Debounce
    }


requestAPI : String -> Cmd Msg
requestAPI value =
    Task.perform SendApiRequest (Task.succeed value)


moveSuggestionCursor : Bool -> Model -> Model
moveSuggestionCursor direction m =
    let
        func =
            if direction then
                Utils.cnext

            else
                Utils.cprev

        overbound =
            if direction then
                -1

            else
                List.length m.suggestions
    in
    case m.selectedSuggestion of
        Nothing ->
            { m | selectedSuggestion = Just (func overbound m.suggestions) }

        Just i ->
            { m | selectedSuggestion = Just (func i m.suggestions) }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Input value ->
            let
                ( debounce, cmd ) =
                    D.push debounceConfig value model.debounce
            in
            ( { model
                | input = value
                , debounce = debounce
              }
            , cmd
            )

        SendApiRequest value ->
            if String.length value > 1 then
                ( model, Geocode.Request.request model value )

            else
                ( resetSuggestions model, none )

        ProcessApiResponse (Ok response) ->
            ( { model | suggestions = parse response }, none )

        ProcessApiResponse (Err _) ->
            ( model, none )

        ShowMarkerOnMap x ->
            ( resetModel True model, Interop.showMarkerOnMap x )

        SetFocus is ->
            ( resetModel False model, none )

        KeyPressed key ->
            case key of
                27 ->
                    ( resetModel True model, none )

                13 ->
                    case model.selectedSuggestion of
                        Nothing ->
                            ( model, none )

                        Just selection ->
                            case Utils.get selection model.suggestions of
                                Nothing ->
                                    ( model, none )

                                Just ( location, _ ) ->
                                    update (ShowMarkerOnMap location) model

                38 ->
                    ( moveSuggestionCursor False model, none )

                40 ->
                    ( moveSuggestionCursor True model, none )

                _ ->
                    ( model, none )

        Debounce msg_ ->
            let
                ( debounce, cmd ) =
                    D.update
                        debounceConfig
                        (D.takeLast requestAPI)
                        msg_
                        model.debounce
            in
            ( { model | debounce = debounce }, cmd )

        Noop ->
            ( model, none )
