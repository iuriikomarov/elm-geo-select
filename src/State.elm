module State exposing (..)

import Debounce as D
import Geocode.Response
import Http
import List
import Platform.Cmd
import Utils


type alias Flags =
    { apiKey : String
    }


type alias Model =
    { input : String
    , suggestions : List Geocode.Response.Suggestion
    , selectedSuggestion : Maybe Int
    , flags : Flags
    , debounce : D.Debounce String
    }


type Msg
    = Noop
    | Input String
    | SendApiRequest String
    | ProcessApiResponse (Result Http.Error String)
    | ShowMarkerOnMap Geocode.Response.Location
    | SetFocus Bool
    | KeyPressed Int
    | Debounce D.Msg


initialModel : Flags -> Model
initialModel flags =
    { input = ""
    , suggestions = []
    , selectedSuggestion = Nothing
    , flags = flags
    , debounce = D.init
    }


initialCommand : Flags -> Cmd Msg
initialCommand _ =
    Platform.Cmd.none


resetModel : Bool -> Model -> Model
resetModel resetInput model =
    resetSuggestions
        { model
            | input =
                if resetInput then
                    ""

                else
                    model.input
        }


resetSuggestions : Model -> Model
resetSuggestions model =
    { model | selectedSuggestion = Nothing, suggestions = [] }
