build: public

run: build
	cd public && python3 -m http.server

public: public/index.js public/index.html

public/index.html: index.html
	@ mkdir -p $(@D)
	@ cp $< $@

public/index.js: $(shell find src -type f -name "*.elm")
	@ mkdir -p $(@D)
	@ elm make src/Main.elm --output=$@

